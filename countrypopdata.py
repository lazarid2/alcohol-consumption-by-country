from bs4 import BeautifulSoup
from bs4 import SoupStrainer
import requests

'''lists'''
country_list = []
population_list = []

url = "https://www.worldometers.info/world-population/population-by-country/"
worldometer = requests.get(url, verify = False)

worldometer_html = worldometer.text

soup = BeautifulSoup(worldometer_html,'html.parser')


#only_a_tags = SoupStrainer("a")
#print(BeautifulSoup(worldometer_html, "html.parser", parse_only=only_a_tags).prettify())

def extract_country_names():
    '''Extract country names from Worldometer'''
    global country_list
    
    for country in soup.find_all("a"):
        print(country)
        #country_list.append(country)
        
    return country_list
    
def extract_country_population():
    '''Extract country pops from Worldometer'''
    global population_list
    
    for population in soup.find_all("td"):
        print(population)
        #country_list.append(country)
        
    return population_list

#extract_country_names()
extract_country_population()

#print(country_list)
#print(population_list)