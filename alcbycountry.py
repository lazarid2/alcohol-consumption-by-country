import re
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter

f = open("HappinessAlcoholConsumption.csv")

'''Lists'''
master = []
countries = []
regions = []
happiness_score = []
HDI = []
gdp_per_cap = []
beer_per_cap = []
spirit_per_cap = []
wine_per_cap = []

'''Master Dictionary'''
master_dictionary = {}

#%% 
def populate_lists():
    '''Populates master list and creates sublists''' 
    global master
    global countries
    global regions
    global happiness_score
    global HDI
    global gdp_per_cap
    global beer_per_cap
    global spirit_per_cap
    global wine_per_cap
    
    '''Populate master list'''
    for lines in f.readlines():
        master.append(lines.split(","))
            
    master = [[re.sub(r"\n",'',element) for element in sublist] for sublist in master] #strip "\n" escape characters from original master list
        
    '''Populate countries list'''
    countries = [sublist[0] for sublist in master]
    countries.pop(0) #remove header line
    
    '''Populate region list'''
    regions = [sublist[1] for sublist in master]
    regions.pop(0)
    
    '''Populate happiness score list'''
    happiness_score = [sublist[3] for sublist in master]
    happiness_score.pop(0)
    
    '''Populate HDI list'''
    HDI = [sublist[4] for sublist in master]
    HDI.pop(0)
    
    '''Populate GDP per capita list'''
    gdp_per_cap = [sublist[5] for sublist in master]
    gdp_per_cap.pop(0)
    
    '''Populate beer per capita list'''
    beer_per_cap = [sublist[6] for sublist in master]
    beer_per_cap.pop(0)
    
    '''Populate spirit per capita list'''
    spirit_per_cap = [sublist[7] for sublist in master]
    spirit_per_cap.pop(0)
    
    '''Populate wine per capita list'''
    wine_per_cap = [sublist[8] for sublist in master]
    wine_per_cap.pop(0)
            
    return (master,countries,regions,happiness_score,HDI,gdp_per_cap,beer_per_cap,spirit_per_cap,wine_per_cap)

populate_lists()
#%%     
def create_master_dictionary():
    '''Create master dictionary of all attributes'''
    global master_dictionary
    
    length = list(range(len(countries)))

    master_dictionary = {countries[n]:{"Region": regions[n], "Happiness": happiness_score[n],"HDI": HDI[n], "GDP per Cap": gdp_per_cap[n], "Beer per Cap.": beer_per_cap[n], "Wine per Cap." : wine_per_cap[n], "Spirit per Cap.": spirit_per_cap[n]} for n in length}
    
    return master_dictionary

create_master_dictionary()
#%%

beer_by_country = list(zip(countries,beer_per_cap))
beer_by_country.sort(key=lambda x: int(x[1]))

wine_by_country = list(zip(countries,wine_per_cap))
wine_by_country.sort(key=lambda x: int(x[1]))

spirits_by_country = list(zip(countries,spirit_per_cap))
spirits_by_country.sort(key=lambda x: int(x[1]))

'''Creating a bar chart for Beer, Wine and Spirit consumption by region'''

labels = [x[0] for x in beer_by_country][30:60:2]
y_pos = np.arange(len(labels))

beer_consumption = [x[1] for x in beer_by_country][30:60:2]

plt.bar(y_pos, beer_consumption, align='center', alpha=0.5, color = 'green')
plt.xticks(y_pos, labels)
matplotlib.pyplot.xticks(fontsize=8)
plt.xticks(rotation=45)
plt.ylabel("Beer per Capita")
plt.xlabel("Country")
plt.title("Beer per Capita by Country")

plt.show()
#print(beer_by_country)

#%%  
'''Take the happiness score for countries within each region, sum them and return the average happiness per region'''
   
region_happiness = list(zip(regions,happiness_score))
region_happiness.sort(key = lambda x: x[0])

region_happiness_dict = {} #dictionary to populate with sum total of happiness score per region 

def region_happiness_average():
    '''Return the average happiness score per region'''
    region_count = dict(Counter(key for key,values in region_happiness)) #counts the occurence of each region in region_happiness list

    '''Aggregate the happiness score for each region (sum of happiness scores from all countries within a region)'''
    for key, value in region_happiness:
        if key in region_happiness_dict:
            region_happiness_dict[key] += float(value)
        else:
            region_happiness_dict[key] = float(value)
    
    '''Divide aggregated happiness score per region by count of region to return average'''
    for regions, hap_total in region_happiness_dict.items():
        for reg, count in region_count.items():
            if regions == reg:
                region_happiness_dict[regions] = round(float(hap_total/count),3)
                
    return region_happiness_dict

region_happiness_average()
            
'''Creating a bar chart for average happiness by by region'''

def plot_region_happiness():

    labels = [x[0] for x in region_happiness_dict.items()]
    y_pos = np.arange(len(labels))
    
    reg_happiness = [x[1] for x in region_happiness_dict.items()]
    
    plt.bar(y_pos, reg_happiness, align='center', alpha=0.5, color = 'blue')
    plt.xticks(y_pos, labels)
    matplotlib.pyplot.xticks(fontsize=8)
    plt.xticks(rotation=90)
    plt.ylabel("Happiness Score")
    plt.xlabel("Region")
    plt.title("Average Happiness per Region")
    plt.grid(axis = 'y')

    plt.show()
                        
plot_region_happiness()


#%%  
region_labels = [x[0] for x in region_happiness_dict.items()]
y = [x[1] for x in region_happiness_dict.items()]
total = sum(y)

plt.pie(y, labels = region_labels,autopct=lambda p:'{:.2f}'.format(p*total/100), shadow = True)
plt.title("Average Happiness Per Region (Happiness Score)")
plt.show()


